<?php
require_once 'functions.php';
$pdo = getConnection();
$statistics = getCompanyStatistics($pdo);
?>
<html>
<body>
    <h1>Atlyginimų statistika</h1>
    <table border="1">
    <tr>
        <th>Darbuotojų skaičius</th>
        <th>Vidutininis atlyginimas</th>
        <th>Mažiausias atlyginimas</th>
        <th>Didžiausias atlyginimas</th>
    </tr>
    <tr>
        <td><?php echo $statistics['employees_count']; ?></td>
        <td><?php echo round($statistics['average_salary'], 2); ?></td>
        <td><?php echo $statistics['min_salary']; ?></td>
        <td><?php echo $statistics['max_salary']; ?></td>
    </tr>
    </table>
</body>
</html>
